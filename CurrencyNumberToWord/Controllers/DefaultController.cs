﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Razor.Internal;
using Newtonsoft.Json;

namespace CurrencyNumberToWord.Controllers
{
    //Default Controller 
    public class DefaultController : Controller
    {
        public IActionResult Index()
        {
            Model.Format form = new Model.Format();

            return View(form);
        }
        [HttpPost]
        public async Task<IActionResult> Result(Model.Format model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            string i = model.FormatedString;
            string Baseurl = "http://localhost:50283/";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync("api/FormatCurrency/" + i);
                var responseString = Res.Content.ReadAsStringAsync().Result;
                i = JsonConvert.DeserializeObject<string>(responseString);
                model.FormatedString = i;
                model.PersonName = model.PersonName;
            }
            return View("Index", model);
        }
    }
}