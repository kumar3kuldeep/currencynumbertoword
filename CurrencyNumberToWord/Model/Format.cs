﻿

using System.ComponentModel.DataAnnotations;

namespace CurrencyNumberToWord.Model
{
    public class Format
    {
        [Required(ErrorMessage = "!!! Bad Request Please Enter Currency GO Back and Enter Again!!!")]
        [RegularExpression(@"[\d]{1,4}([.,][\d]{1,3})?", ErrorMessage = "!!!Value must be numeric with three decimal places only!!!")]
        public string FormatedString { get; set; }

        [Required(ErrorMessage = "!!! Bad Request Please Enter Person Name !!!")]
       
        public string PersonName { get; set; }
    }
}
